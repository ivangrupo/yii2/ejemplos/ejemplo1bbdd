<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Picos */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Picos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="picos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['Guardar', 'id' => $model->nombre], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['Borrar', 'id' => $model->nombre], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Estás seguro de querer borrar esto?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nombre',
            'altitud',
            'sistema',
        ],
    ]) ?>

</div>
