<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Picos */

$this->title = 'Añadir Pico: ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Picos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nombre, 'url' => ['view', 'id' => $model->nombre]];
$this->params['breadcrumbs'][] = 'Guardar';
?>
<div class="picos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
