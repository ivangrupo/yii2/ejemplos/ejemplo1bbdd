<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "picos".
 *
 * @property string $nombre
 * @property int $altitud
 * @property string $sistema
 *
 * @property Eleva[] $elevas
 * @property Provincias[] $provincias
 */
class Picos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'picos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['altitud'], 'integer'],
            [['nombre', 'sistema'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nombre' => 'Nombre del pico',
            'altitud' => 'Altitud',
            'sistema' => 'Sistema montañoso',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getElevas()
    {
        return $this->hasMany(Eleva::className(), ['pico' => 'nombre']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvincias()
    {
        return $this->hasMany(Provincias::className(), ['provincia' => 'provincia'])->viaTable('eleva', ['pico' => 'nombre']);
    }
}
